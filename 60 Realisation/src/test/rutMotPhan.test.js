// import MockFirebase from "mock-cloud-firestore";
// import * as business from "../hoc/business/business";

// const userInfo = {
//   userId: "o5AvgJJF1ggJcVym5ieNQ65EX1y2"
// };
// const fixtureData = {
//   __collection__: {
//     banks: {
//       __doc__: {
//         b1: {
//           fullname: "AsiaCommercialBank",
//           shortname: "acb",
//           unlimitrate: "0.06"
//         },
//         b2: {
//           fullname: "Ngoai thuong VN",
//           shortname: "vcb",
//           unlimitrate: "0.06"
//         }
//       }
//     },
//     endcondition: {
//       __doc__: {
//         e0: { name: "Tai tuc goc va lai" },
//         e1: { name: "Tai tuc goc" },
//         e2: { name: "Tat toan" }
//       }
//     },
//     interestpayment: {
//       __doc__: {
//         p0: { description: "Dau ky", name: "Dau ky" },
//         p1: { description: "Cuoi ky", name: "Cuoi ky" },
//         p2: { description: "Dinh ky", name: "Dinh ky" }
//       }
//     },
//     termend: {
//       __doc__: {
//         e0: { description: "Tai tuc goc & lai", name: "Tai tuc goc & lai" },
//         e1: { description: "Tai tuc goc", name: "Tai tuc goc" },
//         e2: { description: "Tat toan", name: "Tat toan" }
//       }
//     },
//     terms: {
//       __doc__: {
//         t00: { description: "Khong ky han", term: 0 },
//         t01: { description: "Ky han 1", term: 1 },
//         t03: { description: "Ky han 3", term: 3 },
//         t06: { description: "Ky han 6", term: 6 },
//         t12: { description: "Ky han 12", term: 12 }
//       }
//     },
//     passbooks: {
//       __doc__: {
//         passbook00000001: {
//           balance: "10000000",
//           bank_id: "__ref__:/banks/b1",
//           end: false,
//           endcondition_id: "__ref__:/endcondition/e0",
//           enddate: "",
//           interest_payment: "__ref__:/interestpayment/p0",
//           interest_rate: "6",
//           name: "001",
//           opendate: new Date("2018-01-01"),
//           term_id: "__ref__:/terms/t01",
//           unlimit_interest_rate: 0.05,
//           user: "o5AvgJJF1ggJcVym5ieNQ65EX1y2"
//         }
//       }
//     }
//   }
// };

// let firebase = new MockFirebase(fixtureData, {
//   isNaiveSnapshotListenerEnabled: true
// });

// describe("Test rut mot phan", () => {
//   it("trả về id của document", () => {
//     const db = firebase.firestore();

//     const result = db.collection("passbooks").doc("passbook00000001");
//     expect(result.id).toBe("passbook00000001");
//   });

//   it("trả về realBalance của document", async () => {
//     const db = firebase.firestore();
//     const passbooks = await business.loadPassbooks(userInfo);

//     console.log(firebase.auth().currentUser);
//     // expect(passbooks[0].id).toBe("passbook00000001");
//   });
// });

// import {
//   MockFirebase,
//   MockAuthentication,
//   MockFirestore,
//   MockFirebaseSdk
// } from "firebase-mock";
// import * as business from "../hoc/business/business";

// describe("test mock db", () => {
//   let user, auth, db, firebase;
//   beforeEach(() => {
//     user = { uid: "user", email: "abc@abc.abc", password: "@abc12345" };
//     db = new MockFirestore(null, require("./data.json"));
//     auth = new MockAuthentication();
//     auth.autoFlush();
//   });

//   it("sets auth data", () => {
//     auth.changeAuthState(user);
//     expect(auth.getAuth().uid).toBe("user");
//   });

//   it("calls all passbooks data from mocked db", () => {
//     let passbooks = db.collection("passbooks");
//     expect(passbooks.data["passbook00000001"].balance).toBe("10000000");
//   });

//   it("calls all passbooks data from mocked db using business module", async () => {
//     auth.changeAuthState(user);
//     let passbooks = await business.loadPassbooks({ userId: "user" });
//     console.log(passbooks);
//   });
// });
import React from "react";
import Detail from "../container/MoneyContainer/DetailContainer/PassbookDetail";
import Layout from "../hoc/Layout/Layout.js";
import Modal from "../components/UI/Modal/Modal";
import Spinner from "../components/UI/Spinner/Spinner";
import Button from "../components/UI/Button/Button";
import Input from "../components/UI/Input/Input";
import moment from "moment";
import classes from "../container/MoneyContainer/DetailContainer/PassbookDetail.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { formatNum, getEndDate } from "../hoc/business/refineUI";
import {
  loadPassbook,
  saveLog,
  saveNewPassbook,
  updatePassbook
} from "../hoc/business/business";
import {
  checkValidityDetailAction,
  checkAllowDeposit,
  checkAllowWithdraw
} from "../hoc/business/checkValidity";
import withAuthentication from "../hoc/withAuth/withAuthentication";
import * as images from "../hoc/images";

import { mount, shallow, render } from "enzyme";

import * as business from "../hoc/business/business";
var firebasemock = require("firebase-mock");
var mockauth = new firebasemock.MockAuthentication();
var mockfirestore = new firebasemock.MockFirestore(
  null,
  require("./data.json")
);
var mocksdk = new firebasemock.MockFirebaseSdk(
  null,
  () => {
    return mockauth;
  },
  () => {
    return mockfirestore;
  },
  null,
  null
);
const user = { uid: "user", email: "abc@abc.abc", password: "@abc12345" };

describe("Rút một phần", () => {
  it("sets auth data", () => {
    mocksdk.auth().changeAuthState(user);
    mocksdk.auth().flush();
    expect(mocksdk.auth().getAuth().uid).toBe("user");
  });
  it("gets all passbooks from mocked db without auth", () => {
    const passbooks = mocksdk.firestore().collection("passbooks");
    expect(passbooks.data["passbook1"].balance).toBe("10000000");
  });
  it("Rút 1.000.000 STK passbook2 kỳ hạn 1 tháng, rút ngày 20/1/2018", () => {
    const passbook2 = mocksdk
      .firestore()
      .collection("passbooks")
      .doc("passbook2").data;
    const realBalance = business.calculate(
      passbook2.log,
      passbook2.balance,
      parseFloat(passbook2.interest_rate),
      parseFloat(passbook2.unlimit_interest_rate),
      moment(passbook2.opendate).format("DD/MM/YYYY"),
      parseInt(passbook2.term),
      passbook2.end,
      passbook2.enddate !== ""
        ? moment(passbook2.enddate).format("DD/MM/YYYY")
        : "",
      passbook2.interest_payment,
      passbook2.endcondition_id
    );
    expect(realBalance).toBe(9041000);
  });
});

describe("Kiểm tra detail component", () => {
  it("Phải render chính xác", () => {
    const component = mount(<Detail DEBUG={true} />);
    component.setState({
      loading: false,
      error: "",
      isEditing: false,
      isWithdrawing: false,
      isDespositing: false,
      isAccounting: false,
      passbook: {
        id: "001",
        balance: 10000000,
        realBalance: 12000000,
        bankId: "acb",
        bankFullname: "ACB",
        bankShortname: "ACB",
        end: false,
        endDate: "",
        interestRate: "10",
        unlimitInterestRate: "5",
        paymentDesc: "tai tuc goc + lai",
        paymentName: "Tai tuc goc + lai",
        paymentId: "p0",
        passbookName: "001",
        opendate: "01/01/2017",
        termId: "t12",
        term: "12",
        termDes: "12 thang",
        userId: "user",
        idToken: "user",
        log: []
      },
      actionResult: {
        elementType: "input",
        elementConfig: {
          type: "text",
          disabled: false
        },
        value: "",
        validationDeposite: {
          valid: false,
          required: true,
          isGreaterThan0: true
        },
        validationWithdraw: {
          valid: false,
          required: true,
          isGreaterThan0: true,
          isSmallerThanRoot: true
        },
        status: {
          isValid: false,
          errorMessage: ""
        },
        touched: false
      },
      actions: {
        edit: {
          name: "Sửa thông tin",
          config: {
            disabled: false
          },
          icon: images.edit
        },
        withdraw: {
          name: "Rút bớt",
          config: {
            disabled: false
          },
          icon: images.withdraw
        },
        deposit: {
          name: "Gửi thêm",
          config: {
            disabled: false
          },
          icon: images.deposit
        },
        accounting: {
          name: "Tất toán",
          config: {
            disabled: false
          },
          icon: images.accounting
        },
        closing: {
          name: "Thoát",
          config: {
            disabled: false
          },
          icon: images.exit
        }
      }
    });
    expect(component).toMatchSnapshot();
  });

  it("Kiểm tra validate, 0 < rút số tiền < 12.000.000 -> right", () => {
    const component = mount(<Detail DEBUG={true} />);
    component.setState({
      loading: false,
      error: "",
      isEditing: false,
      isWithdrawing: true,
      isDespositing: false,
      isAccounting: false,
      passbook: {
        id: "001",
        balance: 10000000,
        realBalance: 12000000,
        bankId: "acb",
        bankFullname: "ACB",
        bankShortname: "ACB",
        end: false,
        endDate: "",
        interestRate: "10",
        unlimitInterestRate: "5",
        paymentDesc: "tai tuc goc + lai",
        paymentName: "Tai tuc goc + lai",
        paymentId: "p0",
        passbookName: "001",
        opendate: "01/01/2017",
        termId: "t12",
        term: "12",
        termDes: "12 thang",
        userId: "user",
        idToken: "user",
        log: []
      },
      actionResult: {
        elementType: "input",
        elementConfig: {
          type: "text",
          disabled: false
        },
        value: "",
        validationDeposite: {
          valid: false,
          required: true,
          isGreaterThan0: true
        },
        validationWithdraw: {
          valid: false,
          required: true,
          isGreaterThan0: true,
          isSmallerThanRoot: true
        },
        status: {
          isValid: false,
          errorMessage: ""
        },
        touched: false
      },
      actions: {
        edit: {
          name: "Sửa thông tin",
          config: {
            disabled: false
          },
          icon: images.edit
        },
        withdraw: {
          name: "Rút bớt",
          config: {
            disabled: false
          },
          icon: images.withdraw
        },
        deposit: {
          name: "Gửi thêm",
          config: {
            disabled: false
          },
          icon: images.deposit
        },
        accounting: {
          name: "Tất toán",
          config: {
            disabled: false
          },
          icon: images.accounting
        },
        closing: {
          name: "Thoát",
          config: {
            disabled: false
          },
          icon: images.exit
        }
      }
    });
    expect(component.find(Modal)).toHaveLength(2);
    expect(component.find(Button)).toHaveLength(8);
    expect(component.find(Input)).toHaveLength(1);
    component
      .find('input[type="text"]')
      .at(0)
      .simulate("change", { target: { value: "1000000" } });
    // console.log(component.debug());
    // console.log(component.state());
    expect(component.state().actionResult.status.isValid).toBe(true);
  });

  
  it("Kiểm tra validate, rút số tiền <= 0 -> wrong", () => {
    const component = mount(<Detail DEBUG={true} />);
    component.setState({
      loading: false,
      error: "",
      isEditing: false,
      isWithdrawing: true,
      isDespositing: false,
      isAccounting: false,
      passbook: {
        id: "001",
        balance: 10000000,
        realBalance: 12000000,
        bankId: "acb",
        bankFullname: "ACB",
        bankShortname: "ACB",
        end: false,
        endDate: "",
        interestRate: "10",
        unlimitInterestRate: "5",
        paymentDesc: "tai tuc goc + lai",
        paymentName: "Tai tuc goc + lai",
        paymentId: "p0",
        passbookName: "001",
        opendate: "01/01/2017",
        termId: "t12",
        term: "12",
        termDes: "12 thang",
        userId: "user",
        idToken: "user",
        log: []
      },
      actionResult: {
        elementType: "input",
        elementConfig: {
          type: "text",
          disabled: false
        },
        value: "",
        validationDeposite: {
          valid: false,
          required: true,
          isGreaterThan0: true
        },
        validationWithdraw: {
          valid: false,
          required: true,
          isGreaterThan0: true,
          isSmallerThanRoot: true
        },
        status: {
          isValid: false,
          errorMessage: ""
        },
        touched: false
      },
      actions: {
        edit: {
          name: "Sửa thông tin",
          config: {
            disabled: false
          },
          icon: images.edit
        },
        withdraw: {
          name: "Rút bớt",
          config: {
            disabled: false
          },
          icon: images.withdraw
        },
        deposit: {
          name: "Gửi thêm",
          config: {
            disabled: false
          },
          icon: images.deposit
        },
        accounting: {
          name: "Tất toán",
          config: {
            disabled: false
          },
          icon: images.accounting
        },
        closing: {
          name: "Thoát",
          config: {
            disabled: false
          },
          icon: images.exit
        }
      }
    });
    expect(component.find(Modal)).toHaveLength(2);
    expect(component.find(Button)).toHaveLength(8);
    expect(component.find(Input)).toHaveLength(1);
    component
      .find('input[type="text"]')
      .at(0)
      .simulate("change", { target: { value: "0" } });
    // console.log(component.debug());
    // console.log(component.state());
    expect(component.state().actionResult.status.isValid).toBe(false);
  });

  
  it("Kiểm tra validate, rút số tiền > 12.000.000 -> false", () => {
    const component = mount(<Detail DEBUG={true} />);
    component.setState({
      loading: false,
      error: "",
      isEditing: false,
      isWithdrawing: true,
      isDespositing: false,
      isAccounting: false,
      passbook: {
        id: "001",
        balance: 10000000,
        realBalance: 12000000,
        bankId: "acb",
        bankFullname: "ACB",
        bankShortname: "ACB",
        end: false,
        endDate: "",
        interestRate: "10",
        unlimitInterestRate: "5",
        paymentDesc: "tai tuc goc + lai",
        paymentName: "Tai tuc goc + lai",
        paymentId: "p0",
        passbookName: "001",
        opendate: "01/01/2017",
        termId: "t12",
        term: "12",
        termDes: "12 thang",
        userId: "user",
        idToken: "user",
        log: []
      },
      actionResult: {
        elementType: "input",
        elementConfig: {
          type: "text",
          disabled: false
        },
        value: "",
        validationDeposite: {
          valid: false,
          required: true,
          isGreaterThan0: true
        },
        validationWithdraw: {
          valid: false,
          required: true,
          isGreaterThan0: true,
          isSmallerThanRoot: true
        },
        status: {
          isValid: false,
          errorMessage: ""
        },
        touched: false
      },
      actions: {
        edit: {
          name: "Sửa thông tin",
          config: {
            disabled: false
          },
          icon: images.edit
        },
        withdraw: {
          name: "Rút bớt",
          config: {
            disabled: false
          },
          icon: images.withdraw
        },
        deposit: {
          name: "Gửi thêm",
          config: {
            disabled: false
          },
          icon: images.deposit
        },
        accounting: {
          name: "Tất toán",
          config: {
            disabled: false
          },
          icon: images.accounting
        },
        closing: {
          name: "Thoát",
          config: {
            disabled: false
          },
          icon: images.exit
        }
      }
    });
    expect(component.find(Modal)).toHaveLength(2);
    expect(component.find(Button)).toHaveLength(8);
    expect(component.find(Input)).toHaveLength(1);
    component
      .find('input[type="text"]')
      .at(0)
      .simulate("change", { target: { value: "12000001" } });
    // console.log(component.debug());
    // console.log(component.state());
    expect(component.state().actionResult.status.isValid).toBe(false);
  });
});
